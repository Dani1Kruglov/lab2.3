#include <iostream>
#include<cmath>


double f(double x)
{
    return(log(x) - 1);
}

double Bisection(double a, double b,  double eps)
{
    while( f(b) - f(a) > eps)
    {
        double x;
        x = (a + b) / 2;
        if( ( f(x)  == a ) && ( f(x) == b))
            return x;
        else if(f(x) * f(a) > 0)
            a = x;
        else
            b= x;
    }
}


int main()
{
    double eps= 1e-4;
    std::cout<<Bisection( 0, 10, eps);
    return 0;
}
